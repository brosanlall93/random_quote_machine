
const randomQuote = require('random-quote');
 
require("./resources/style.css");


const refreshClick = () =>{
	$.getJSON("http://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=30&callback=", function(a) {
  		//get a random index
  		var ind = Math.floor(Math.random()*30);
  		$('#quote').html(a[ind].content);
  		$('#author').html("<p>— " + a[ind].title + "</p>");
  		//$('body').append(a[ind].content + "<p>— " + a[ind].title + "</p>");
	});
}

$(document).ready(function(){
	$('#refresh').click(refreshClick);

	//Just to make a quote appear on page load, call refreshClick
	refreshClick();
});

